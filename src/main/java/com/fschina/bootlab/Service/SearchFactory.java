package com.fschina.bootlab.Service;

import com.fschina.bootlab.spi.SendMessage;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * @program: bootlab
 * @description: 查询服务查找类
 * @author: QinYanqing
 * @create: 2018-09-26 15:07
 **/
public class SearchFactory {

        private SearchFactory(){

        }

        public static SendMessage getSender(){
            SendMessage sendMessage =null;
            ServiceLoader<SendMessage> serviceLoader=ServiceLoader.load(SendMessage.class);
            Iterator<SendMessage> iterator=serviceLoader.iterator();
            while (iterator.hasNext()){
                sendMessage = iterator.next();
                sendMessage.send();
            }
            return  sendMessage;
        }
}
