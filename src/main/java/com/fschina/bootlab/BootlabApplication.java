package com.fschina.bootlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootlabApplication.class, args);
	}
}
