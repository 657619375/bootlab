package com.fschina.bootlab.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: bootlab
 * @description: springboot demo
 * @author: QinYanqing
 * @create: 2018-08-07 09:33
 **/
@RestController
@EnableAutoConfiguration
public class FirstSpringBootController {

    @RequestMapping("/hello")
    private String sayHello(){
        return "hello world!";
    }
}
