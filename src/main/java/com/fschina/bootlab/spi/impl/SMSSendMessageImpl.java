package com.fschina.bootlab.spi.impl;


import com.fschina.bootlab.spi.SendMessage;

/**
 * @program: dubboboot
 * @description: 文件
 * @author: QinYanqing
 * @create: 2018-09-17 13:51
 **/
public class SMSSendMessageImpl implements SendMessage {
    @Override
    public String send() {
        System.out.println("send sms to user");
        return "send sms success!";
    }
}
