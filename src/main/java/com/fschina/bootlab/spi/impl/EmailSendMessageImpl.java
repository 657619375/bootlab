package com.fschina.bootlab.spi.impl;


import com.fschina.bootlab.spi.SendMessage;

/**
 * @program: dubboboot
 * @description: 数据库
 * @author: QinYanqing
 * @create: 2018-09-17 13:53
 **/
public class EmailSendMessageImpl implements SendMessage {
    @Override
    public String send() {
        System.out.println("send email to user");
        return "send email success!";
    }
}
