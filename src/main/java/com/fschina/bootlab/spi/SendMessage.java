package com.fschina.bootlab.spi;

public interface SendMessage {

    public String send();
}
